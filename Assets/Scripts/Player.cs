using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    HealthComponent health;
    ArmorComponent armor;
    public GameObject hitScreen;
    public Image armorUI;
    public Image healthUI;
    public GameObject damageEffect;
    public GameObject glowEffect;
    public GameObject dieMenu;
    bool isTakingDamage = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        health = GetComponent<HealthComponent>();
        armor = GetComponent<ArmorComponent>();
        UIManager.Instance.UpdateHealthText((int)health.GetCurrentHealth());
        damageEffect.SetActive(false);
    }

    
    void Update()
    {   
        // // Reduce hit screen color
        // if(hitScreen != null)
        // {
        //     if(hitScreen.GetComponent<Image>().color.a > 0)
        //     {
        //         var color = hitScreen.GetComponent<Image>().color;
        //         color.a -= 0.01f;
        //         hitScreen.GetComponent<Image>().color = color;
        //     }
        // }

        if(armor.GetCurrentArmor() != armor.MaxArmor && !armor.isRegen)
        {
            armor.StartArmorRegen();
        }

        // Hit Effect
        if(isTakingDamage)
        {
            StartCoroutine("PlayDamageEffect");
        }
    }

    public void TakeDamage(float amount)
    {
        if(armor.isRegen)
        {
            armor.StopArmorRegen();
        }

        // Hit Screen effect
        // var color = hitScreen.GetComponent<Image>().color;
        // color.a = 0.5f;

        // hitScreen.GetComponent<Image>().color = color;
        isTakingDamage = true;


        //Save the amount of damage remaining off.
        float damageLeft = amount;

        // Should armor damage be calculated?
        if (armor.GetCurrentArmor() > 0)
        { 
            int armorDamage = (int)Mathf.Min(damageLeft, armor.GetCurrentArmor());
            armor.ReduceArmor(armorDamage);
            damageLeft -= armorDamage;
            armorUI.fillAmount = armor.GetCurrentArmor() / armor.MaxArmor;
        }

        if (damageLeft >= 0)
        {
            health.ReduceHealth(damageLeft);
            healthUI.fillAmount = health.GetCurrentHealth() / health.MaxHealth;
        }

        if (health.GetCurrentHealth() <= 0)
        {
            // Die
            Time.timeScale = 0;
            dieMenu.SetActive(true);
            Cursor.visible = true;
        }
    }

    public void AddHealth(int num)
    {
        health.AddHealth(num);
        healthUI.fillAmount = health.GetCurrentHealth() / health.MaxHealth;
    }

    void EnableDamageEffect()
    {
        glowEffect.SetActive(false);
        damageEffect.SetActive(true);
    }

    void DisableDamageEffect()
    {
        damageEffect.SetActive(false);
        glowEffect.SetActive(true);
    }

    IEnumerator PlayDamageEffect()
    {      
        EnableDamageEffect();
        yield return new WaitForSeconds(0.1f);
        DisableDamageEffect();
        isTakingDamage = false;
    }
}
