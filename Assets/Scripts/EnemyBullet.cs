using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float Damage;
    public float LifeSpan;
    public float BulletSpeed;
    public Rigidbody2D Rb;
    Vector3 dir;
    GameObject player;
    GameObject owner;
    void Start()
    {
        player = GameObject.Find("Player");
        dir = player.transform.position - transform.position;
        dir.Normalize();
        dir *= BulletSpeed;
        Destroy(this.gameObject, LifeSpan);
    }


    void Update()
    {
        Rb.velocity = dir * BulletSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player player = collision.gameObject.GetComponent<Player>();

            if (player) player.TakeDamage(Damage);

            //Debug.Log("Hit Player");
            Destroy(gameObject);
        }
        else if(collision.gameObject.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }
    }

    public void SetOwner(GameObject obj)
    {
        owner = obj;
    }

    void RegisterIndicator()
    {
        if(owner)
        {
            if (!DI_System.CheckIfObjectInSight(owner.transform))
            {
                DI_System.CreateIndicator(owner.transform);
            }
        }
    }

}
