using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TargetDetection : MonoBehaviour
{
    public UnityEvent<GameObject> OnPlayerHit;
    public GameObject Parent;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>())
        {
            OnPlayerHit.Invoke(Parent);
        }
    }

}
