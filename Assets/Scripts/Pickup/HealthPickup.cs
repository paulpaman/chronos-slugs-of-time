using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : Pickup
{
    public int HealthToAdd;

    protected override void ApplyEffect(Player player)
    {
        base.ApplyEffect(player);
        player.AddHealth(HealthToAdd);
        Destroy(gameObject);
    }
}
