using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FireType { SingleFire,Automatic};
public enum BulletType { Single,Spread};
public class Gun : MonoBehaviour
{
    [SerializeField] FireType fireType;
    [SerializeField] BulletType bulletType;

    [Header("General Attributes")]
    public Animator GunAnim;
    public GameObject BulletImpactPrefab;
    int currentAmmo;
    int totalAmmo;
    public int MaxAmmo;
    public int ClipSize;
    public int WeaponDamage;
    public float FireRate;
    private float nextTimeToFire = 0f;
    public float recoilSpread;
    bool canReload = false;
    bool shooting;

    [Header("Spread Type Only")]
    public int BulletsPerShot;
 

    Camera playerCam;
    PlayerController player;


    void OnEnable()
    {
        //currentAmmo = ClipSize;
        UIManager.Instance.UpdateClipSizeText(currentAmmo);
    }

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        if (player) playerCam = player.ViewCam;
        currentAmmo = ClipSize;
        UIManager.Instance.UpdateClipSizeText(currentAmmo);
        //totalAmmo = MaxAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        switch(fireType)
        {
            case FireType.SingleFire:
                shooting = Input.GetMouseButtonDown(0);
                break;
            case FireType.Automatic:
                shooting = Input.GetMouseButton(0);
                break;
            default:
                break;
        }

        if (shooting && Time.time >= nextTimeToFire)
        {
            if (currentAmmo > 0)
            {
                nextTimeToFire = Time.time + 1f / FireRate;
                //Shoot();
                GunAnim.SetTrigger("Shoot");
               
            }
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            if(currentAmmo < ClipSize)
            {
                Reload();
            } 
        }
    }

    public void Shoot()
    {
       switch(bulletType)
        {
            case BulletType.Single:
                SingleBullet();
                break;
            case BulletType.Spread:
                MultipleBullet();
                break;
        }
    }

    void SingleBullet()
    {
        float x = Random.Range(-recoilSpread, recoilSpread);
        float y = Random.Range(-recoilSpread, recoilSpread);

        Ray ray = playerCam.ViewportPointToRay(new Vector3(0.5f + x, 0.5f + y, 0f));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            Instantiate(BulletImpactPrefab, hit.point, transform.rotation);
            if (hit.transform.CompareTag("Enemy"))
            {
                Enemy enemy = hit.transform.GetComponent<Enemy>();
                if (enemy)
                {
                    enemy.TakeDamage(WeaponDamage);
                }
            }
        }
        else
        {

        }
        currentAmmo--;
        UIManager.Instance.UpdateClipSizeText(currentAmmo);
        //GunAnim.SetTrigger("Shoot");
    }

    void MultipleBullet()
    {
        for(int i = 0; i < BulletsPerShot; i++)
        {
            float x = Random.Range(-recoilSpread, recoilSpread);
            float y = Random.Range(-recoilSpread, recoilSpread);

            Ray ray = playerCam.ViewportPointToRay(new Vector3(0.5f + x, 0.5f + y, 0f));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Instantiate(BulletImpactPrefab, hit.point, transform.rotation);
                if (hit.transform.CompareTag("Enemy"))
                {
                    Enemy enemy = hit.transform.GetComponent<Enemy>();
                    if (enemy)
                    {
                        enemy.TakeDamage(WeaponDamage);
                    }
                }
            }
            else
            {

            }
        }
        currentAmmo--;
        UIManager.Instance.UpdateClipSizeText(currentAmmo);
        //GunAnim.SetTrigger("Shoot");
    }

    void Reload()
    {
        if (WeaponController.Instance.TotalAmmo >= ClipSize - currentAmmo)
        {
            GunAnim.SetTrigger("Reload");
            WeaponController.Instance.TotalAmmo -= (Mathf.Clamp(ClipSize - currentAmmo, 0, WeaponController.Instance.MaxAmmo));
            currentAmmo = ClipSize;
           
        }
        if(WeaponController.Instance.TotalAmmo < ClipSize - currentAmmo)
        {
            currentAmmo += WeaponController.Instance.TotalAmmo;
            WeaponController.Instance.TotalAmmo = 0;
        }

        UIManager.Instance.UpdateAmmoText(WeaponController.Instance.TotalAmmo);
        UIManager.Instance.UpdateClipSizeText(currentAmmo);
    }
}
