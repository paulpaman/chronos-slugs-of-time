using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Enemy : MonoBehaviour
{
    [HideInInspector] public NavMeshAgent agent;
    [SerializeField] Material hitMaterial;
    Material defaultMaterial;
    SpriteRenderer spriteRenderer;
    Player player;

    public float PlayerRange = 10f;
    public float ShootingRange = 3.5f;
    public bool CanShoot;
    public float FireRate;
    float shotCounter;
    public GameObject BulletPrefab;
    public Transform FirePoint;
    bool isShooting = false;
    float oldPos;
    public int Health;

    public int MeleeDamage;

    [HideInInspector] public bool isHit = false;
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        NavMesh.avoidancePredictionTime = 4.0f;
        player = GameObject.Find("Player").GetComponent<Player>();

        spriteRenderer = GetComponent<SpriteRenderer>();
        defaultMaterial = spriteRenderer.material;
    }

    void Update()
    {
        if(isShooting)
        {
            if (Vector3.Distance(transform.position, player.transform.position) < PlayerRange)
            {
                if (CanShoot)
                {
                    shotCounter -= Time.deltaTime;
                    if (shotCounter <= 0)
                    {
                        EnemyBullet bullet =  Instantiate(BulletPrefab, FirePoint.position, FirePoint.rotation).GetComponent<EnemyBullet>();
                        bullet.SetOwner(this.gameObject);
                        shotCounter = FireRate;
                    }
                }
            }
        }
    }

    private void LateUpdate()
    {
        oldPos = transform.position.x;
    }

    public void MoveTo(Transform objToMoveTo)
    {
        agent.SetDestination(objToMoveTo.position);
    }

    public void TakeDamage(int damage)
    {
        isHit = true;
        Health -= damage;
        spriteRenderer.material = hitMaterial;
        if (Health <= 0f)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Invoke("ResetMaterial", 0.1f);
        }
    }

    public void StopAgent(bool b)
    {
        agent.isStopped = b;
    }

    public void StartShooting()
    {
        isShooting = true;
    }

    public void StopShooting()
    {
        isShooting = false;
        CanShoot = true;
        shotCounter = FireRate;
    }

    public Vector3 GetNextPos()
    {
        return agent.nextPosition;
    }

    public float GetOldPosX()
    {
        return oldPos;
    }

    private void ResetMaterial()
    {
        spriteRenderer.material = defaultMaterial;
    }

    public void MeleeAttack()
    {
        if(IsPlayerInRange(ShootingRange,player.transform))
        {
            player.TakeDamage(MeleeDamage);
        }
    }

    bool IsPlayerInRange(float range, Transform player)
    {

        if (player)
        {
            return Vector3.Distance(this.transform.position, player.position) <= range;
        }

        return false;
    }

}
