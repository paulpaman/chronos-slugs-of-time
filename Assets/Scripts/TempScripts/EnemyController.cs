using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public int Health;


    public float PlayerRange = 10f;

    public Rigidbody2D Rb;
    public float MoveSpeed;

    public bool CanShoot;
    public float FireRate;
    float shotCounter;
    public GameObject BulletPrefab;
    public Transform FirePoint;

    GameObject player;
    void Start()
    {
        player = GameObject.Find("Player");
    }

    
    void Update()
    {
        if(Vector3.Distance(transform.position,player.transform.position) < PlayerRange)
        {
            Vector3 playerDir = player.transform.position - transform.position;
            Rb.velocity = playerDir.normalized * MoveSpeed;

            if(CanShoot)
            {
                shotCounter -= Time.deltaTime;
                if(shotCounter<=0)
                {
                    Instantiate(BulletPrefab, FirePoint.position, FirePoint.rotation);
                    shotCounter = FireRate;
                }
            }
        }
        else
        {
            Rb.velocity = Vector2.zero;
        }
    }

    public void TakeDamage(int damage)
    {
        Health -= damage;
        if(Health <= 0f)
        {
            Destroy(this.gameObject);
        }
    }
}
