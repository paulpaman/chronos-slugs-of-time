using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrap : MonoBehaviour
{
    public float Damage;
    public Animator animator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if(player)
            {
                player.TakeDamage(Damage);
            }
        }
    }

    public void TriggerTrap()
    {
        animator.SetTrigger("Trap");
    }
}
