using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicateDamage : MonoBehaviour
{
    public RectTransform indicator; // Indicator, an image on canvas.
    public Transform otherTransform; // Your other object, in case you use position of firing as source
    public Transform player; // your player transform.
    public bool useDirection; // Only needed for demo.
    float angle = 0;

    private void Update()
    {
        angle = GetHitAngle(player, otherTransform.forward);
        indicator.rotation = Quaternion.Euler(0, 0, -angle);
    }


    public float GetHitAngle(Transform player, Vector3 incomingDir)
    {
        // Flatten to plane
        var otherDir = new Vector3(-incomingDir.x, 0f, -incomingDir.z);
        var playerFwd = Vector3.ProjectOnPlane(player.forward, Vector3.up);

        // Direction between player fwd and incoming object
        var angle = Vector3.SignedAngle(playerFwd, otherDir, Vector3.up);

        return angle;
    }
}
