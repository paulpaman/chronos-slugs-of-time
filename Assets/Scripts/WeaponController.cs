using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    private static WeaponController _instance;

    public static WeaponController Instance { get { return _instance; } }

    public int MaxAmmo;
    [HideInInspector]public int TotalAmmo;

    public int selectedWeapon = 0;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        TotalAmmo = MaxAmmo;
        UIManager.Instance.UpdateAmmoText(TotalAmmo);
        SelectWeapon();
    }

    
    void Update()
    {
        
        if(Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            selectedWeapon = (selectedWeapon + 1) % transform.childCount;
            SelectWeapon();
        }
        if(Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            selectedWeapon = (selectedWeapon + (transform.childCount - 1)) % transform.childCount;
            SelectWeapon();
        }
    }

    void SelectWeapon()
    {
        int i = 0;
        foreach(Transform weapon in transform)
        {
            if(i == selectedWeapon)
            {
                weapon.gameObject.SetActive(true);
            }
            else
            {
                weapon.gameObject.SetActive(false);
            }
            i++;
        }
    }

    public void AddTotalAmmo(int num)
    {
        TotalAmmo += num;
        if(TotalAmmo >= MaxAmmo)
        {
            TotalAmmo = MaxAmmo;
        }
        if (UIManager.Instance) UIManager.Instance.UpdateAmmoText(TotalAmmo);
    }
}
